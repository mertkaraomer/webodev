﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using odeevv.Models;

namespace odeevv.Controllers
{
    public class KAYITController : Controller
    {
        private KAYITEntities db = new KAYITEntities();

        // GET: KAYIT
        public ActionResult Index()
        {
            return View(db.KAYIT.ToList());
        }

        // GET: KAYIT/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KAYIT kAYIT = db.KAYIT.Find(id);
            if (kAYIT == null)
            {
                return HttpNotFound();
            }
            return View(kAYIT);
        }

        // GET: KAYIT/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: KAYIT/Create
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KULLANICIID,AD,SOYAD,E_MAIL,SİFRE,DOGUMTARIHI")] KAYIT kAYIT)
        {
            if (ModelState.IsValid)
            {
                db.KAYIT.Add(kAYIT);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(kAYIT);
        }

        // GET: KAYIT/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KAYIT kAYIT = db.KAYIT.Find(id);
            if (kAYIT == null)
            {
                return HttpNotFound();
            }
            return View(kAYIT);
        }

        // POST: KAYIT/Edit/5
        // Aşırı gönderim saldırılarından korunmak için, lütfen bağlamak istediğiniz belirli özellikleri etkinleştirin, 
        // daha fazla bilgi için https://go.microsoft.com/fwlink/?LinkId=317598 sayfasına bakın.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KULLANICIID,AD,SOYAD,E_MAIL,SİFRE,DOGUMTARIHI")] KAYIT kAYIT)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kAYIT).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(kAYIT);
        }

        // GET: KAYIT/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KAYIT kAYIT = db.KAYIT.Find(id);
            if (kAYIT == null)
            {
                return HttpNotFound();
            }
            return View(kAYIT);
        }

        // POST: KAYIT/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            KAYIT kAYIT = db.KAYIT.Find(id);
            db.KAYIT.Remove(kAYIT);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
